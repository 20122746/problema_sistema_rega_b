class AdjacentEdge:

    def __init__(self, to_label: str, weight: int):
        self._to_label: str = to_label
        self._weight: int = weight
        self._visited = False

    def get_to_label(self) -> str:
        return self._to_label

    def get_weight(self):
        return self._weight

    def set_visited(self, is_visited: bool):
        self._visited = is_visited

    def __str__(self):
        return str(self._to_label) + " : " + str(self._weight) + " " + str(self._visited)