from Graph import LinkedGraph


def rede1():
    g = LinkedGraph()
    vertices = ["A", "C", "D", "E"]
    for label in vertices:
        g.add_vertex(label)
    vertices_pump = ["B", "F"]
    for label in vertices_pump:
        g.add_vertex(label, True)
    edges = [("A", "B", 2), ("A", "C", 8), ("A", "E", 7), ("B", "C", 5), ("B", "D", 7),
             ("C", "D", 9), ("C", "E", 8), ("D", "F", 4), ("E", "F", 3)]
    for edge in edges:
        g.add_edge(edge[0], edge[1], edge[2])
    return g


rede = rede1()

print(rede)

rede.get_minimum_spanning_tree("A")
