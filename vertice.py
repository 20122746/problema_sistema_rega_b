from edge import AdjacentEdge


class Vertex:

    def __init__(self, label: str, pump: bool = False):
        self._label: str = label
        self._visited: bool = False
        self._adjacent_list = []    # o vertice é criado com uma lista de adjagencias vazia
        self._pump = pump

    def get_label(self) -> str:         # retorna a label
        return self.label

    def get_pump(self):         # retorna True se for bomba, False se não for
        return self._pump

    def get_adjacents_vertices(self):   # retorna a lista de vertices adjacentes
        result = []
        for edge in self._adjacent_list:
            result.append(edge.get_to_label())
        return self._adjacent_list

    def get_adjacents_edges(self):
        return self._adjacent_list

    def add_adjacent_edge(self, to_label: str, weight: int):
        new_edge = AdjacentEdge(to_label, weight)
        self._adjacent_list.append(new_edge)

    def is_visited(self) -> bool:   # permite saber se um determinado vertice já foi visitado ou não
        return self._visited

    def set_visited(self, is_visited : bool): # define se o vertice ja foi visitado ou nao
        self._visited = is_visited
