from vertice import Vertex

"""Classe para o TDA grafo"""


class LinkedGraph:

    def __init__(self):
        """ Define o estado inicial de self"""
        self._vertices: dict[str] = {}

    # métodos gerais das coleções

    def is_empty(self):
        """Retorna True se len(self) é 0, senão False """
        return len(self._vertices) == 0

    def __len__(self):
        """Devolve numero de vértices de self"""
        return len(self._vertices)

    def __str__(self):
        """Devolve a representacao em string de self"""

        result = ""
        for vertex in self._vertices:
            result = result + str(vertex) + "\n"
            adj = self._vertices[vertex].get_adjacents_vertices()
            for edge in adj:
                result = result + "\t" + str(edge) + "\n"
        return result

    def clear(self):
        """ Torna self vazio"""
        self._vertices = {}

    def __iter__(self):
        """ Suporta a iteração sobre self """
        return iter(self._vertices)

    # métodos específicos do grafo

    def size_vertices(self):
        """Devolve o número de vértices"""
        return len(self._vertices)

    def size_edges(self):
        """Devolve o número de arestas """
        count = 0
        for v in self._vertices:
            count += len(self._vertices[v])
        return count

    def get_vertices(self):
        """Devolve o conjunto dos vértices """
        return self._vertices.keys()

    def adjacents(self, label):
        """Devolve o conjunto de vértices  adjacentes de label"""
        if label in self._vertices:
            return self._vertices[label].get_adjacents_vertices()
        return None

    def adjacent_edges(self, label):
        """Devolve o conjunto de arestas com origem em label"""

        if label in self._vertices:
            return self._vertices[label].get_adjacents_edges()
        return None

    def add_vertex(self, label: str, pump=False) -> None:
        """ Adiciona vértice a self
        précondição: label não é vértice de self
        póscondição: label faz parte do conjunto de vértices de self"""
        if label not in self._vertices:
            self._vertices[label] = Vertex(label, pump)

    def add_edge(self, from_label: str, to_label: str, weight: int) -> None:
        """ Adiciona aresta a self
        précondição: a aresta a adicionar não faz parte do conjunto de arestas de self
        póscondição: a aresta integra o conjunto de arestas de self"""
        if from_label in self._vertices and to_label in self._vertices:
            if to_label not in self.adjacents(from_label):
                self._vertices[from_label].add_adjacent_edge(to_label, weight)
                self._vertices[to_label].add_adjacent_edge(from_label, weight)

    def get_vertices_with_pump(self):
        lst = []
        for label in self._vertices:
            if self._vertices[label].get_pump():
                lst.append(label)
        return lst

    def get_minimum_spanning_tree(self, label: str):
        vertices = self._vertices
        for v in vertices:
            vertex = vertices[v]
            vertex.set_visited(False)   # marca todos os vertices como não visitados
            edges = vertex.get_adjacents_edges()
            for e in edges:
                e.set_visited(False)    # marca todas as arestas como não visitadas

        linear_edge_collection = []
        vertex = vertices[label]   # vertice inicial
        edges = vertex.get_adjacents_edges()    # arestas do vertice inicial
        for e in edges:
            linear_edge_collection.append(e)

        num_vertex = self.size_vertices()
        k = 1
        menor_custo = 0

        g_resultado = LinkedGraph()

        while k < num_vertex:
            edge_menor_custo = linear_edge_collection[0]
            for e in edges:     # determinar qual a aresta de menor custo
                if e.get_weight() < edge_menor_custo.get_weight():
                    edge_menor_custo = e

            linear_edge_collection.remove(edge_menor_custo) # retirar a aresta de menor custo

            w = vertices[edge_menor_custo.get_to_label()] # determinar o vertice de destino
            if not w.is_visited():
                w.set_visited(True)                      # definir como visitado
                edge_menor_custo.set_visited(True)
                # adiciona o vertice ao resultado
                g_resultado.add_vertex(w.get_label(), w.get_pump())
                # adiciona a aresta ao resultado
                g_resultado.add_edge(vertex.get_label(), edge_menor_custo.get_to_label, edge_menor_custo.get_weight)

                menor_custo += edge_menor_custo.get_weight()
                k += 1

                vertex = vertices[vertex.get_label()]
                edges = vertex.get_adjacents_edges()
                for e in edges:
                    if vertices[e.get_to_label()].is_visited():



        return g_resultado, menor_custo



